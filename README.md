# WH-interview-tasks

## Task 01

Please prepare the real layout based on this psd file (to open this file you have to use Photoshop or if you don't have this tool you can use it the free online version - https://www.photopea.com/). The layout should be responsive (RWD). You have to propose your version for mobile and tablets views. Additionally you should add the following cool features like:
- sticky top menu bar (with logo)
- smooth scrolling to the sections after clicking on the menu link
- in "Gallery" section after clicking "VIEW MORE" button load another images below these existing one. You can use it one of this generators (i.e. https://loremipsum.io/21-of-the-best-placeholder-image-generators/)
- in "Blog" section after clicking "VIEW MORE" button load another blog posts using one of fake api (i.e. https://jsonplaceholder.typicode.com/)
- in "Contact" section please add a simple client-side validation to the form. Validation should work on the blur events on the fields and also on the submit event on the 'Send button'*. It will be good to show some kindly information about the problem for the users, for example directly below the field. Validation rules for fields:
	- name field (only letters and  -. signs are valid)
	- email field (should validate a proper email address)
	- phone number (only numbers and +- signs are valid)
	- message field (field without any restrictions)

*Please add also a custom 'Send button' to the Contact form - the designer has forgotten about this ;)

Please feel free to add any other features that you would like to show off – this is not required but would be a good way of demonstrating your skills and ideas.

